/**
 * Created by Tomek on 25.10.2015.
 */
$(document).ready(function(){
    $('.v-menu').hide();
    $('.cover').hide();
    $('.wait').hide();
    $('.about').hide();
    if(window.location.hash != '')
        doHash();

    $('.nav li > a').click(function(event){
        $('.h-menu li').removeClass();
        $(this).parent().addClass('active');
        var menu = $(this).next();
        $('.v-menu').not(menu).slideUp(200);
    });

    $('.nav ul > a').click(function(event){
        $('.v-menu').slideUp(200);
    });

    $('.dropdown').click(function(event) {
        event.preventDefault();
        $(this).next().slideToggle(200);
    });

    $('.page').click(function() {
        $('.v-menu').hide();
    });
});

function doHash() {
    $('.wait').hide();
    if(window.location.hash == "#info") {
        $(".parallax").hide();
        $(".about").show();
        $(".about p").hide();
        $(".about p:first-child").fadeIn(500);
        $(".about p:nth-child(2)").fadeIn(1000);
        $(".about p:nth-child(3)").fadeIn(1500);
        $(".cover").hide();


    } else {
        $('.wait').show().delay(1000);
        $('.parallax').load(window.location.hash.replace('#', '')+".html", function(responseText, textStatus, XMLHttpRequest) {
            $('.wait').hide();
            $(".parallax").show();
            $(".about").hide();
            $(".cover").fadeIn(500);

            $(".comment").each(function(){
                var comment = localStorage.getItem($(this).attr("id"));
                if(comment != null) {
                    $(this).text(comment);
                    $(this).prev().val(comment);
                } else {
                    $(this).text('');
                    $(this).prev().val('');
                }
            });

            $('.menu-item').hover(function () {
                $(this).css('color', 'white');
            }, function () {
                $(this).css('color', 'inherit');
            });

            $('.parallax-group').on('mouseenter', function () {
                $(this).find('img').css('-webkit-filter', 'blur(0px) grayscale(0%)');
                $(this).find('img').css('filter', 'blur(0px) grayscale(0%)');
            });

            $('.parallax-group').on('mouseleave', function () {
                $(this).find('img').css('-webkit-filter', 'blur(3px) grayscale(100%)');
                $(this).find('img').css('filter', 'blur(3px) grayscale(100%)');
            });

            $('.description').on('dblclick', function () {
                var $comment = $(this).find('.comment');
                $(this).find('textarea').val($comment.text());
                $(this).find('textarea').show().focus();
                $comment.hide();
            });

            $('textarea').on('focusout', function () {
                var $area = $(this).hide();
                var $comment = $(this).next().show();
                $comment.text($area.val());
                localStorage.setItem($comment.attr("id"), $area.val());
            });

            $(document).on('keypress', function (e) {
                if (e.which == 13) {
                    $('textarea').focusout();
                }
            });
        });
    }
}